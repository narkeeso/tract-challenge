import React, { useContext, useEffect, useState } from "react";
import Firebase from "firebase";
import {
  extendTheme,
  Box,
  ChakraProvider,
  Container,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalOverlay,
  Input,
  ModalFooter,
  Button,
} from "@chakra-ui/core";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

import { FirebaseContext, FirebaseProvider } from "./context";

import useFirebaseAuth from "./hooks/useFirebaseAuth";
import useYouTubeVideoData from "./hooks/useYouTubeVideoData";

import theme from "./theme";

import Header from "./components/Header";
import LiveScreen from "./components/Live";

import "stream-chat-react/dist/css/index.css";
import "./App.css";
import useLocalStorage from "./hooks/useLocalStorage";

function App() {
  const [videoId, setVideoId] = useState("");
  const [videoIdValue, setVideoIdValue] = useState("");
  const [isChatEnabled, toggleChat] = useLocalStorage("chat:enabled", true);

  const firebase = useContext(FirebaseContext);
  const authUser = useFirebaseAuth(firebase);
  const videoData = useYouTubeVideoData(videoId);
  const customTheme = extendTheme(theme);

  const uiConfig = {
    signInFlow: "popup",
    signInOptions: [Firebase.auth.GoogleAuthProvider.PROVIDER_ID],
    callbacks: {
      signInSuccess: (currentUser, credential, redirectURL) => {
        window.location.reload();
        return false;
      },
    },
  };

  useEffect(() => {
    const videoIdPath = window.location.pathname.substring(1);

    if (videoIdPath) {
      setVideoId(videoIdPath);
    }
  }, []);

  return (
    <ChakraProvider theme={customTheme}>
      <FirebaseProvider>
        <Header isChatEnabled={isChatEnabled} toggleChat={toggleChat} />
        <Box>
          <Container maxWidth="2048px">
            {authUser && videoData ? (
              <LiveScreen
                authUser={authUser}
                videoData={videoData}
                videoId={videoId}
                isChatEnabled={isChatEnabled}
                toggleChat={toggleChat}
              />
            ) : (
              <Modal isOpen={true} isCentered>
                <ModalOverlay />
                <ModalContent>
                  <ModalHeader>
                    {authUser ? "Join Session" : "Sign In"}
                  </ModalHeader>
                  <ModalBody>
                    {!authUser && (
                      <StyledFirebaseAuth
                        uiConfig={uiConfig}
                        firebaseAuth={firebase.auth()}
                      />
                    )}
                    {!videoId && authUser && (
                      <Input
                        placeholder="YouTube Video ID"
                        value={videoIdValue}
                        onChange={(event) =>
                          setVideoIdValue(event.target.value)
                        }
                      />
                    )}
                  </ModalBody>
                  {!videoId && authUser && (
                    <ModalFooter>
                      <Button
                        colorScheme="blue"
                        width="100%"
                        onClick={() => {
                          setVideoId(videoIdValue);
                          window.history.pushState({}, null, videoIdValue);
                        }}
                      >
                        Join
                      </Button>
                    </ModalFooter>
                  )}
                </ModalContent>
              </Modal>
            )}
          </Container>
        </Box>
      </FirebaseProvider>
    </ChakraProvider>
  );
}

export default App;
