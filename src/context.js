import React, { createContext } from "react";
import firebase from "firebase";

firebase.initializeApp(window.firebaseConfig);

export const FirebaseContext = createContext(firebase);

export const FirebaseProvider = (props) => {
  return (
    <FirebaseContext.Provider value={firebase}>
      {props.children}
    </FirebaseContext.Provider>
  );
};
