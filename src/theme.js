const config = {
  useSystemColorMode: false,
  initialColorMode: "light",
};

const Button = {
  baseStyle: {
    borderRadius: "50px",
  },
  variants: {
    outline: {
      border: "2px solid",
    },
  },
};

export default {
  config,
  components: {
    Button,
  },
  colors: {
    blue: {
      50: "#e7e7ff",
      100: "#b8bbfe",
      200: "#898ef6",
      300: "#5a61f1",
      400: "#2c34ec",
      500: "#131bd3",
      600: "#0c15a5",
      700: "#070e77",
      800: "#020849",
      900: "#00021e",
    },
  },
};
