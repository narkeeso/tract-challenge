import { StreamChat } from "stream-chat";

const chatClient = new StreamChat(process.env.REACT_APP_STREAM_CHAT_KEY);

export default chatClient;
