/** @jsx jsx */

import { css, jsx } from "@emotion/core";
import styled from "@emotion/styled";

import {
  Container,
  Flex,
  IconButton,
  Switch,
  Text,
  Tooltip,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/core";
import { BiArrowFromRight } from "react-icons/bi";

const Header = styled.header`
  height: 70px;
  box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.15);
  position: sticky;
  top: 0;
  margin-bottom: 20px;
  z-index: 99999;
`;

const Logo = styled.img`
  height: 40px;
`;

export default function ({ isChatEnabled, toggleChat }) {
  const { colorMode, toggleColorMode } = useColorMode();
  const bg = useColorModeValue("white", "gray.900");

  return (
    <Header>
      <Container maxWidth="100%" height="100%" background={bg}>
        <Flex alignItems="center" justifyContent="space-between" height="100%">
          <Logo
            src={colorMode === "dark" ? "/tract_dark.svg" : "/tract_light.svg"}
            alt="Tract Live"
          />
          <Flex alignItems="center">
            <Switch
              onChange={toggleColorMode}
              isChecked={colorMode === "dark"}
            />
            <Text
              css={css`
                margin: 0 8px;
                position: relative;
                top: -1px;
              `}
            >
              Dark Mode
            </Text>
            {!isChatEnabled && (
              <Tooltip label="Expand Chat">
                <IconButton
                  as={BiArrowFromRight}
                  variant="ghost"
                  size="sm"
                  css={css`
                    position: relative;
                    top: -2px;
                  `}
                  onClick={() => toggleChat(true)}
                />
              </Tooltip>
            )}
          </Flex>
        </Flex>
      </Container>
    </Header>
  );
}
