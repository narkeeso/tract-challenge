/** @jsx jsx */

import { useEffect, useState } from "react";
import {
  Chat,
  Channel,
  Thread,
  Window,
  MessageLivestream,
} from "stream-chat-react";
import { MessageList, MessageInput } from "stream-chat-react";
import { BiArrowFromLeft } from "react-icons/bi";

import chatClient from "../chat";

import {
  AspectRatio,
  Avatar,
  Button,
  Center,
  Divider,
  Flex,
  Grid,
  GridItem,
  Heading,
  IconButton,
  Tag,
  Text,
  Tooltip,
  useBreakpointValue,
  useColorMode,
} from "@chakra-ui/core";
import { css, jsx } from "@emotion/core";

import useLocalStorage from "../hooks/useLocalStorage";

function LiveScreen({
  authUser,
  videoData,
  videoId,
  isChatEnabled,
  toggleChat,
}) {
  const [channel, setChatChannel] = useState(null);
  const isMobile = useBreakpointValue({ base: true, md: false });

  // Persist saving videos and following users
  const [isVideoSaved, setSaveVideo] = useLocalStorage(
    `video:id:${videoId}`,
    false
  );
  const [isFollowing, setFollow] = useLocalStorage(
    `user:id:${videoData.channelId}`,
    false
  );

  const { colorMode } = useColorMode();

  // Setup Chat Channel
  useEffect(() => {
    chatClient.setUser(
      {
        id: authUser.uid,
        name: authUser.displayName,
        image: authUser.photoURL,
      },
      chatClient.devToken(authUser.uid)
    );

    const chan = chatClient.channel("messaging", videoId, {});

    setChatChannel(chan);
  }, [videoId, authUser.uid, authUser.displayName, authUser.photoURL]);

  return (
    <Grid
      templateColumns={isMobile ? "auto" : "auto 350px"}
      css={css`
        height: 100%;
        padding-bottom: 50px;
      `}
      gap={5}
    >
      <GridItem colSpan={isChatEnabled ? 1 : 2}>
        <AspectRatio ratio={16 / 9}>
          <iframe
            title="tract-live"
            width="560"
            height="315"
            src={`https://www.youtube.com/embed/${videoId}`}
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </AspectRatio>
        <Flex
          alignItems="center"
          css={css`
            padding: 30px 0 15px 0;
          `}
        >
          <div
            css={css`
              margin-right: 10px;
            `}
          >
            <Avatar
              name={videoData.author}
              src={videoData.authorThumbnailURL}
              size="lg"
              css={
                videoData.isLive
                  ? css`
                      border: 3px solid #2f855a;
                    `
                  : null
              }
            >
              {videoData.isLive && (
                <Tag
                  size="sm"
                  css={css`
                    background-color: #2f855a;
                    border: 2px solid #fff;
                    color: #fff;
                    font-weight: bold;
                    position: absolute;
                    top: 50px;
                  `}
                >
                  Live
                </Tag>
              )}
            </Avatar>
          </div>
          <div>
            <Heading size="md">{videoData.title}</Heading>
            <Heading size="sm" color="gray.500">
              Hosted by {videoData.author} &bull; 4:30 - 5:00 PDT
            </Heading>
          </div>
          <div
            css={css`
              margin-left: auto;
              & > * {
                margin-left: 10px;
              }
            `}
          >
            <Button variant="outline" onClick={() => setFollow(!isFollowing)}>
              {isFollowing ? "Unfollow" : "Follow"}
            </Button>
            <Button
              colorScheme="blue"
              onClick={() => setSaveVideo(!isVideoSaved)}
            >
              {isVideoSaved ? "Saved" : "Save Video"}
            </Button>
          </div>
        </Flex>
        <Divider
          css={css`
            margin: 10px 0;
          `}
        />
        <Text
          colorScheme="gray.600"
          css={css`
            white-space: pre-line;
          `}
        >
          {videoData.description}
        </Text>
      </GridItem>
      {isChatEnabled && (
        <GridItem
          bg={colorMode === "dark" ? "gray.800" : "white"}
          colSpan={1}
          css={
            !isMobile
              ? css`
                  align-self: flex-start;
                  position: sticky;
                  top: 90px;
                  height: calc(100vh - 170px);
                `
              : css`
                  height: calc(100vh - 150px);
                  opacity: 0.95;
                  position: absolute;
                  left: 0;
                  top: 75px;
                  width: 100%;
                `
          }
        >
          <div
            css={css`
              padding: 20px 0;
              position: relative;
            `}
          >
            <Tooltip label="Collapse Chat">
              <IconButton
                as={BiArrowFromLeft}
                onClick={() => toggleChat(false)}
                variant="ghost"
                size="sm"
                css={css`
                  left: 10px;
                  position: absolute;
                  top: 50%;
                  transform: translateY(-50%);
                `}
              />
            </Tooltip>
            <Center>
              <h2
                css={css`
                  font-size: 18px;
                `}
              >
                Live Chat
              </h2>
            </Center>
          </div>
          <Divider />
          {channel && (
            <Chat client={chatClient} theme={`messaging ${colorMode}`}>
              <Channel channel={channel} Message={MessageLivestream}>
                <Window hideOnThread>
                  <MessageList />
                  <MessageInput focus />
                </Window>
                <Thread />
              </Channel>
            </Chat>
          )}
        </GridItem>
      )}
    </Grid>
  );
}

export default LiveScreen;
