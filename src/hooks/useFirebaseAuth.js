import { useEffect, useState } from "react";

const useFirebaseAuth = (firebase) => {
  const [authUser, setAuthUser] = useState(null);

  useEffect(() => {
    const unlisten = firebase.auth().onAuthStateChanged((authUser) => {
      authUser ? setAuthUser(authUser) : setAuthUser(null);
    });
    return () => {
      unlisten();
    };
  }, [firebase]);

  return authUser;
};

export default useFirebaseAuth;
