import { useEffect, useState } from "react";

const apiKey = process.env.REACT_APP_YOUTUBE_API_KEY;

const useYouTubeVideoData = (videoId) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    if (!videoId) {
      return;
    }

    const getVideoData = async () => {
      const videoRes = await fetch(
        `https://www.googleapis.com/youtube/v3/videos?id=${videoId}&key=${apiKey}&part=snippet`
      );

      const videoJSON = await videoRes.json();
      const videoData = videoJSON.items[0].snippet;

      const channelRes = await fetch(
        `https://www.googleapis.com/youtube/v3/channels?part=snippet&id=${videoData.channelId}&key=${apiKey}`
      );

      const channelJSON = await channelRes.json();
      const channelData = channelJSON.items[0].snippet;

      return {
        title: videoData.title,
        description: videoData.description,
        author: channelData.title,
        authorThumbnailURL: channelData.thumbnails.default.url,
        channelId: videoData.channelId,
        isLive: videoData.liveBroadcastContent === "live",
      };
    };

    getVideoData().then(setData);
  }, [videoId]);

  return data;
};

export default useYouTubeVideoData;
